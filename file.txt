1. The logger prints to a file instead of to a console, and gives information as to the time,
	the level of log, and the package, class and method the log came from. It also shows the
	FINE logs.

2. JUNIT sends the message to the logger after testing has had its condition been evaluated.
   Its fine because the logger in the logger.properties is accepting all levels of log.
   It is done by logger through running the tests. We get the line because there is only one
   logger throughout the entire scope.

3. It means the the test will be considered a success (a pass) if an exception is thrown.

4. 
    1. The serializableUID is used to ensure that other objects serializing it have the same objects.
	    Otherwise, and InvalidClassException would be thrown. 
	2. It needs to overwrite the constructor because the class it extends, Exception, has a constructor,
	   and it is necessary for an inherited class to have its own constructor.
	3. Because we only need the constructor, and the other classes aren't virtual, so don't need to
	   be overwritten unless we wanted to, which we don't.
	   
5. The static block is used to configure the logger file statically for all instances of the class,
   before the constructor is called. this is to ensure logging through all aspects of the code,
   even the constructor.
   
6. A README.md file is run on the source page of remote repositories. It allows others to understand the
   contents of the repository as well as being a summary of the project. The format is how the file
   will be presented on the repositories, instead of just being plain text. It allows for more unique
   formatting to be shown.
   
7. The test is failing because it could not throw the exception. Something is occurring in the finally block
   that is stopping the throwing of the TimeException class, another runtime exception is being thrown.
   
8. The actual problem is that after the exception is thrown, it goes to the finally block and prints the data,
    but since the timeNow variable is set to null, and is not set to a value because an exception is thrown before
	then, the finally block tries to preform a subtraction operation on a null variable, throwing another exception,
	which is not the one specified in the assertThrows in the Junit test.
	
9. 

10.

11. Our TimerException class is extended from Exception. It is throwable and is checked. NullPointerException is a
    RuntimeException which means it is also throwable and unchecked.